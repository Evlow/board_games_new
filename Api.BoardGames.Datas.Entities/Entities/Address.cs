﻿using System;
using System.Collections.Generic;

namespace Api.BoardGames.Datas.Entities.Entities
{
    public partial class Address
    {
        public Address()
        {
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string? NumberStreet { get; set; }
        public string? Street { get; set; }
        public string? CodePost { get; set; }
        public string? City { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
