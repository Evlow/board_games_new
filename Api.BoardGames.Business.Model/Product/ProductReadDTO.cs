﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.BoardGames.Business.Model.Product
{
    public class ProductReadDTO : ProductBaseDTO
    {
        public int ProductId { get; set; }
    }
}

