﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.BoardGames.Business.Model.Product
{

    /// <summary>
    /// Model de base d'un produit
    /// </summary>

    public class ProductBaseDTO
    {
        /// <summary>
        /// Le nom d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Le prix d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public double Price { get; set; }


        /// <summary>
        /// La description d'un produit.
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// L'image d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public string Picture { get; set; }
        /// <summary>
        /// Le stock d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public double Stock { get; set; }
        /// <summary>
        /// La date de création d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// La date de mise à jour d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public DateTime UpdatedAt { get; set; }
        /// <summary>
        /// Id de la categorie d'un produit
        /// </summary>
        /// <value>
        /// The name of the departement.
        /// </value>
        public int CategoryId { get; set; }

    }
}