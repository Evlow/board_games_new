﻿//using Api.BoardGames.Datas.Context.Contract;
//using Api.BoardGames.Datas.Repository;
//using Api.BoardGames.Datas.Repository.Contract;

//namespace Api.BoardGames.IoC.WebApi
//{

//        public static class IoCApplication
//        {
//            public static IServiceCollection ConfigureInjectionDependencyRepository(this IServiceCollection services)
//            {
//                services.AddScoped<IRepositoryAddress, RepositoryAddress>();
//                services.AddScoped<IRepositoryCategory, RepositoryCategory>();
//                services.AddScoped<IRepositoryOrder, RepositoryOrder();
//                services.AddScoped<IRepositoryProduct, RepositoryProduct>();
//                services.AddScoped<IRepositoryProductOrder, RepositoryProductOrder>();
//                services.AddScoped<IRepositoryRole, RepositoryRole>();
//                services.AddScoped<IRepositoryState, RepositoryState>();
//                services.AddScoped<IRepositoryUser, RepositoryUser>();
//            return services;
//            }

//            public static IServiceCollection ConfigureDBContext(this IServiceCollection services, IConfiguration configuration)
//            {
//                var connectionString = configuration.GetConnectionString("BddConnection");

//                services.AddDbContext<IBoardGamesContext, BoardGamesDBContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
//                    .LogTo(Console.WriteLine, LogLevel.Information)
//                    .EnableSensitiveDataLogging()
//                    .EnableDetailedErrors());

//                return services;
//            }


//        }
//    }
