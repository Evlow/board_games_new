﻿using Api.BoardGames.Datas.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Api.BoardGames.Datas.Context.Contract
{
    public interface IBoardGamesContext : IDbContext
    {
        DbSet<Address> Addresses { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<ProductOrder> ProductOrders { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<State> States { get; set; }
        DbSet<User> Users { get; set; }


    }
}
